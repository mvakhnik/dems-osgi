/**
 * 
 */
package com.vmk.dems.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author max
 *
 */
public class JsonUtil {

	
	public static String toJson(Object pojo) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		return gson.toJson(pojo);
	}
}
