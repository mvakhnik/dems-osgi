/**
 * 
 */
package com.vmk.dems.test;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import static org.junit.Assert.fail;
/**
 * @author max
 *
 */
@Component(immediate = true)
public class App {


	@Activate
	public  void init() {
		System.out.println("START TEST:" + this);
		
		fail("Test not passed");
	}
}
