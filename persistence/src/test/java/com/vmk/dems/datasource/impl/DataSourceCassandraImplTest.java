package com.vmk.dems.datasource.impl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.junit.Test;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.vmk.dems.datasource.PersistenceConstants;

public class DataSourceCassandraImplTest {

	@Test
	public void testGetSession() {
		DataSourceCassandraImpl ds = new DataSourceCassandraImpl();
		Session session = ds.getSession();
		assertNotNull("Session is not created", session);

//		StringBuilder table = new StringBuilder(PersistenceConstants.KEYSPACE_DEMS).append(".test");
//
//
//
//		
//		StringBuilder prepare = new StringBuilder("INSERT INTO ").append(table)
//				.append(" (")
////				.append("id")
//				.append("url")
//				.append(", ")
//				.append("title")
////				.append(", ")
//				
//				.append(")")
//				.append("VALUES (?, ?")
////				.append(", ")
////				.append("?")
//				.append(");");
//		
//		int max = 1000;
//		
//		int BATCH_MAX = 1000;
//		
//		//create table
//		StringBuilder createTable = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(table).append("(")
////				.append("id uuid")
////				.append(",")
//				.append("title text")
//				.append(", ")
//				.append("url text")
//				.append(", ")
//				.append("PRIMARY KEY (")
////				.append("id")
////				.append(", ")
//				.append("url")
//				.append(")")
//				.append(");");
//		session.execute(createTable.toString());
//		
////		Collection<Statement> batches = new ArrayList<>();
//		System.out.println("start batching...");
//		PreparedStatement ps = session.prepare(prepare.toString());
//		
//		{
//			long prepTime = 0;
//			long exTime = 0;
//			
//			for (int a = 0; a < max; a++) {
//				long s0 = System.currentTimeMillis();
//				Collection<Statement> statements = new ArrayList<>();
////				System.out.println("batching:" + a);
//				for (int i = 0; i < BATCH_MAX; i++) {
//					StringBuilder url = new StringBuilder("http://url.dot.com/number/")
//							.append(a)
//							.append("/")
//							.append(i);
//					StringBuilder title = new StringBuilder("Title of url #")
//							.append(a)
//							.append("-")
//							.append(i);
////					UUID uuid = UUID
////							.nameUUIDFromBytes(url.toString().getBytes());
//					BoundStatement binded = ps.bind(url.toString()
//							, title.toString());
//					statements.add(binded);
//				}
//				BatchStatement batch = new BatchStatement();
//				batch.addAll(statements);
////				batches.add(batch);
//				long e0 = System.currentTimeMillis();
//				
//				prepTime = prepTime + (e0 - s0);
//				
//				long s1 = System.currentTimeMillis();
//				session.execute(batch);
//				long e1 = System.currentTimeMillis();
//				exTime = exTime + (e1 - s1);
//				System.out.println("executed:" + a);
//			}
//			
//			
//			
//			System.out.println("time of preparation:" + prepTime);
//			System.out.println("all time of execution:" + exTime);
//		}
//		
	}

}
