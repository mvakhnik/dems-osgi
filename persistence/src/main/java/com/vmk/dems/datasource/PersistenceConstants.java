/**
 * 
 */
package com.vmk.dems.datasource;

/**
 * @author "Maksim Vakhnik"
 *
 */
public interface PersistenceConstants {

	public final static String KEYSPACE_DEMS = "demotivators";
	public final static String KEYSPACE_TEST = "demotivators-test";
}
