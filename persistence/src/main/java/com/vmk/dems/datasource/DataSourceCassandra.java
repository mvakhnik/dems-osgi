package com.vmk.dems.datasource;

import com.datastax.driver.core.Session;

public interface DataSourceCassandra {

	public Session getSession();
}
