package com.vmk.dems.datasource.impl;

import java.util.Arrays;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.vmk.dems.datasource.DataSourceCassandra;
import com.vmk.dems.datasource.PersistenceConstants;

/**
 * DataStax Academy Sample Application
 *
 * Copyright 2013 DataStax
 *
 * This is a Singleton class that holds 1 Cassandra session that all requests
 * will share. It has 1 public method to return that session.
 *
 *
 *
 *
 */
@Service
@Component(immediate = true)
@Properties({@Property(name = org.osgi.framework.Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, value = "sun.misc")})
public class DataSourceCassandraImpl implements DataSourceCassandra{
	private final static String KEYSPACE = "demotivators";
	private final static String PROP_HOST = "CASSANDRA_HOSTS";
	private static final String DEF_HOST = "127.0.0.1";
	//
	// A static variable that holds the session. Only one of these will exist
	// for the whole application
	//

	private static final Logger log = LoggerFactory
			.getLogger(DataSourceCassandraImpl.class.getName());
	
	private static Session cassandraSession = null;
	
	@Activate
	public void init() {
		log.debug("property:{}", System.getProperty("io.netty.noUnsafe"));
		log.debug("Activate cassandra:{}", this);
//		App.Test("test string");
		cassandraSession = createSession();
		log.debug("createSession:{}", cassandraSession);
	}

	/**
	 *
	 * Return the Cassandra session. When the application starts up, the session
	 * is set to null. When this function is called, it checks to see if the
	 * session is null. If so, it creates a new session, and sets the static
	 * session.
	 *
	 * All of the DAO classes are subclasses of this
	 *
	 * @return - a valid cassandra session
	 */

	@Override
	public Session getSession() {
		if (cassandraSession == null) {
			cassandraSession = createSession();
		}
		return cassandraSession;
	}

	/**
	 *
	 * Create a new cassandra Cluster() and Session(). Returns the Session.
	 *
	 * @return A new Cassandra session
	 */

	protected   Session createSession() {
		String[] hosts = getHosts();
		log.debug("Hosts for cassandra:{}", Arrays.deepToString(hosts));
		
		Cluster cluster = Cluster.builder().addContactPoints(hosts).build();
		StringBuilder sb = new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ")
				.append(PersistenceConstants.KEYSPACE_DEMS)
				.append(" WITH REPLICATION={ 'class' : 'SimpleStrategy', 'replication_factor' : 1 }");
		cluster.connect().execute(sb.toString());
		log.debug("Cluster:{}", String.valueOf(cluster));
		createKeyspaceIfNotExist();
		log.debug("Cluster connects to:{}", PersistenceConstants.KEYSPACE_DEMS);
		return cluster.connect(PersistenceConstants.KEYSPACE_DEMS);
	}

	private void createKeyspaceIfNotExist() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Gets from system properties or just localhost.
	 * @return
	 */
	private  String[] getHosts() {
		String hosts = System.getProperty(PROP_HOST, DEF_HOST);
		String[] result = hosts.split(",");
		return result;
	}
}
