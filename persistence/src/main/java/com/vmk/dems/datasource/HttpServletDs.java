/**
 * 
 */
package com.vmk.dems.datasource;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author max
 *
 */
@Component(immediate = true)
@Service
@Properties({@Property(name = "alias", value = "/persistence")})
public class HttpServletDs extends HttpServlet{

	private static final Logger log = LoggerFactory
			.getLogger(HttpServletDs.class.getName());
	
//	@Reference
	private DataSourceCassandra datasource;
	/**
	 * 
	 */
	private static final long serialVersionUID = 201509201021L;

	@Activate
	public void init() {
		log.debug("ACTIVATE:{}", this);
//		TestDataSource ds = new TestDataSource();
//		Session session = ds.getSession();
//		log.debug("SESSION:{}", session);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.getWriter().write("datasource:" + datasource);
	}


	public void bindDatasource(DataSourceCassandra datasource) {
		log.debug("BIND:{}", datasource);
		this.datasource = datasource;
	}
	
	public void unbindDatasource(DataSourceCassandra datasource) {
		log.debug("UNBIND:{}", datasource);
		this.datasource = datasource;
	}
	
	

}
