package com.vmk.dems.core.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.vmk.dems.core.IDemotivatorsSource;

@Component
@Service
public class DemotivatorsSourceImpl implements IDemotivatorsSource{

	public String getCurrent() {
		
		return String.valueOf(this);
	}

}
