package com.vmk.dems.core;

import java.util.Date;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    public void start(BundleContext context) throws Exception {
    	
        System.out.println("start1:" + this + ":" + new Date(System.currentTimeMillis()));
    }

    public void stop(BundleContext context) throws Exception {
    	System.out.println("stop1:" + this + ":" + new Date(System.currentTimeMillis()));
    }

}
