package com.vmk.dems.core.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.log.LogService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component(immediate = true)
@Service
@Properties({@Property(name = "alias", value = HttpServletTest.ALIAS)})
public class HttpServletTest extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 201509142025L;
	public  static final String ALIAS = "/helloworld";

	@Reference
	private LogService logService;

	static class TestGson {
		private String name;
		private String value;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		TestGson object = new TestGson();
		object.setName("object");
		object.setValue(String.valueOf(logService));
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		String json = gson.toJson(object);
		resp.getWriter().write(json);
	}
}
