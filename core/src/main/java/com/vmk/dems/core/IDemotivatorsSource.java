package com.vmk.dems.core;

public interface IDemotivatorsSource {

	/**
	 * Gets current link for current user.
	 * @return
	 */
	public String getCurrent();
}
