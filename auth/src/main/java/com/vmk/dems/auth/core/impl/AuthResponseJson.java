/**
 * 
 */
package com.vmk.dems.auth.core.impl;

/**
 * Authorization response.
 * @author max
 *
 */
public class AuthResponseJson {
	
	private final AuthentificationResult result;

	public AuthResponseJson(AuthentificationResult result) {
		this.result = result;
	}
	public AuthentificationResult getResult() {
		return result;
	}

//	public void setResult(AuthorizationResult result) {
//		this.result = result;
//	}
	
}
