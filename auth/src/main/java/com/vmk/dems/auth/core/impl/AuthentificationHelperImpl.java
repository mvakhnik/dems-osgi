/**
 * 
 */
package com.vmk.dems.auth.core.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.naming.AuthenticationException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vmk.dems.auth.core.AuthenticationHelper;
import com.vmk.dems.auth.dao.IUserDAO;
import com.vmk.dems.auth.model.User;

/**
 * @author max
 *
 */
@Component
@Service
public class AuthentificationHelperImpl implements AuthenticationHelper{

	private static final Logger log = LoggerFactory
			.getLogger(AuthentificationHelperImpl.class.getName());
	
	
	private final static String ALGORITM = "SHA-512";
	private final static String SALT = "d8e8fca2dc0f896fd7cb4cb0031ba249  -";

	@Reference
	private IUserDAO userResource;
	/**
	 * Authenticates user.
	 * 
	 * @param name
	 * @param password
	 *            , not encoded
	 * @return
	 */
	@Override
	public  AuthentificationResult authenticate(String name, String password)
			throws AuthenticationException{
		if (name == null || name.isEmpty() 
				|| password == null || password.isEmpty()) {
			throw new AuthenticationException("Wrong credentials:"
					+ " login and password can not be null or empty");
		}
		User user = userResource.getUser(name);
		byte[] encPassword;
		try {
			encPassword = encodePassword(password);
			byte[] storedPassword = user.getPassword();
			if (Arrays.equals(storedPassword, encPassword)) {
				return AuthentificationResult.SUCCESS;
			}
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
			throw new AuthenticationException("Internal error:"
					+ " no such algoritm:" + ALGORITM);
		}

		return AuthentificationResult.FAILED;
	}

	public static byte[] encodePassword(String password)
			throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(ALGORITM);
		log.debug("password:{}", password);
		md.update(password.getBytes());
		log.debug("first hash:{}", md.digest());
		md.update(SALT.getBytes());
		byte[] digest = md.digest();
		log.debug("result hash:{}", digest);
		return digest;
	}
}
