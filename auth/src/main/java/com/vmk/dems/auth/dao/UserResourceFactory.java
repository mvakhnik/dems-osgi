/**
 * 
 */
package com.vmk.dems.auth.dao;

/**
 * @author max
 *
 */
public class UserResourceFactory {

	private static UserResourceFactory instance =  new UserResourceFactory();
	private IUserDAO userResource;
	
	private UserResourceFactory() {
		
	}
	
	
	public static IUserDAO getUserResource() {
		
		instance.init();
		return instance.userResource;
	}


	private  synchronized void init() {
		if (instance.userResource == null) {
			userResource = new UserResourceCassandra();
		}
	}
}
