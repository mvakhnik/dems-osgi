/**
 * 
 */
package com.vmk.dems.auth.core.impl;

import java.io.IOException;

import javax.naming.AuthenticationException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vmk.dems.auth.core.AuthenticationHelper;
import com.vmk.dems.auth.core.AuthorizationHelper;
import com.vmk.dems.common.JsonUtil;

/**
 * @author max
 *
 */
@Component(immediate = true)
@Service
@Properties({@Property(name = "alias", value = "/login")})
public class HttpServletAuthorization extends HttpServlet{

	public static final String nameField = "name";
	public static final String passField = "password";
	/**
	 * 
	 */
	private static final long serialVersionUID = 201509201021L;

	private static final Logger log = LoggerFactory
			.getLogger(HttpServletAuthorization.class.getName());
	@Reference
	private AuthenticationHelper authenticator;
	@Reference 
	private AuthorizationHelper authorHelper;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String id = req.getSession().getId();
		
		resp.getWriter().write("auth:" + id);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//get login pass
		String name = req.getParameter(nameField);
		String pass = req.getParameter(passField);
		AuthentificationResult authRes = authentificate(name, pass);
		//check creds
		if (authRes == AuthentificationResult.SUCCESS) {
			String sessionId = req.getSession().getId();
			authorHelper.storeAuthorization(name, sessionId);
		}

		//store this session id as authent
		
	resp.getWriter().write(JsonUtil.toJson(authRes));
	}



	private AuthentificationResult authentificate(String name, String password) {
		AuthentificationResult result;
		try {
			result = authenticator.authenticate(name, password);
			
			return result;
		} catch (AuthenticationException e) {
			log.debug("Authentification failed", e);
		}
		return AuthentificationResult.FAILED;
	}
	
	

}
