/**
 * 
 */
package com.vmk.dems.auth.core;

/**
 * @author max
 *
 */
public interface AuthorizationHelper {

	/**
	 * Binds authorized session to user
	 * @param userId
	 * @param sessionId
	 */
	public  void storeAuthorization(String userId, String sessionId);
	
	/**
	 * Checks authorization for this session.
	 * @param sessionId
	 * @return
	 */
	public  boolean isAuthorized(String sessionId);
}
