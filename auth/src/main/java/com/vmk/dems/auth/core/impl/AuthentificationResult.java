package com.vmk.dems.auth.core.impl;

public enum AuthentificationResult {
	SUCCESS,
	FAILED
}