/**
 * 
 */
package com.vmk.dems.auth.dao;

import com.vmk.dems.auth.model.User;


/**
 * @author max
 *
 */
public interface IUserDAO {

	public User getUser(String name);
	public boolean createUser(User user);
}
