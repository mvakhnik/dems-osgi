/**
 * 
 */
package com.vmk.dems.auth.core;

import javax.naming.AuthenticationException;

import com.vmk.dems.auth.core.impl.AuthentificationResult;

/**
 * Authenticate user.
 * 
 * @author max
 *
 */
public interface AuthenticationHelper {
	public AuthentificationResult authenticate(String name, String password)
			throws AuthenticationException;
}
