package com.vmk.dems.auth.core;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.junit.Test;

import com.vmk.dems.auth.core.impl.AuthentificationHelperImpl;

public class AuthentificationHelperTest {

	@Test
	public void testEncodePassword() throws NoSuchAlgorithmException {
		String password = "password";
		byte[] encodePassword = AuthentificationHelperImpl.encodePassword(password);
		assertFalse("Encoded password is same as not encoded", 
				Arrays.equals(password.getBytes(), encodePassword));
	}

}
