package com.vmk.dems.logger;

import org.osgi.service.log.LogService;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;

/**
 * {@link Appender} implementation to OSGI LogService.
 * 
 * @author max
 *
 */

public class AppenderOsgi extends AsyncAppender {

	private LogService getLogService() {
		return LogServiceFactory.getLogService();
	}

	@Override
	public void doAppend(ILoggingEvent logEvent) {

		LogService logService = getLogService();
		if (logService != null) {

			log(logService, logEvent);
		}
	}

	private void log(LogService logService, ILoggingEvent logEvent) {
		Level level = logEvent.getLevel();
		int key = level.levelInt;
		String message = logEvent.getFormattedMessage();
		switch (key) {
		case Level.INFO_INT:
			logService.log(LogService.LOG_INFO, message);
			break;
		case Level.DEBUG_INT:
			logService.log(LogService.LOG_DEBUG, message);
			break;
		case Level.ERROR_INT:
			logService.log(LogService.LOG_ERROR, message);
			break;
		case Level.WARN_INT:
			logService.log(LogService.LOG_WARNING, message);
			break;
		default:
			break;
		}
	}

}
