/**
 * 
 */
package com.vmk.dems.logger;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.service.log.LogService;

/**
 * 
 * Binds/unbinds LogService.
 * @author max
 *
 */
@Component(immediate = true)
public class LogServiceFactory {
	@Reference
	private static LogService logService;
	
	static LogService getLogService() {
		return logService;
	}
	
	public void bindLogService(LogService logService) {
		LogServiceFactory.logService = logService;
	}
	
	public void unbindLogService(LogService logService) {
		LogServiceFactory.logService = null;
	}
}
